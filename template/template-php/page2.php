<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="maqe test template with php">
    <meta name="author" content="George">
    <meta name="robots" content="no index, no follow">

    <title>MAQE template test using PHP</title>

    <!-- CSS -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
   <link rel="stylesheet" href="style.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://maqe.github.io/template.html">MAQE template test</a>
                <a class="navbar-brand" href="http://maqe.github.io/maqe-bot.html">MAQE bot test</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                  <li>
                      <a href="http://bot.maqetest.tk">Solved bot test with node-js</a>
                  </li>
                  <li>
                      <a href="https://bitbucket.org/t4l0s/maqe-tests/src">source at bitbucket</a>
                  </li>
              </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1>MAQE template test</h1>
                <h2>MAQE forums</h2>
                <h3>Subtitle</h3>
                <h4>Posts</h4>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div id="posts" class="col-lg-12">
                <ul>
                    <?php
                    define('PAGINATION',8);
                    $page=2;
                    $data = file_get_contents("posts.json");
                    $posts = json_decode($data,true);
                    $data = file_get_contents("authors.json");
                    $authors = json_decode($data,true);
                    /* free memory ,maybe json files are big*/
                    unset($data);
                    $current=0;

                    function plural($pl){
                        $result='';
                        if ($pl>1){
                            $result='s';
                        }
                        return $result;
                    }

                    for ($current=0+8*($page-1);$current<min ($page*PAGINATION,sizeof($posts)) ;$current++){
                    $auth_id=$posts[$current]['author_id']-1;
                    $post_date=date_create(explode(' ',$posts[$current]['created_at'])[0]);
                    $current_date=date_create(date('Y-m-d'));
                    $diff=date_diff($post_date, $current_date);
                    $age='';

                    while (true){
                    if ($diff->y!=0){
                        $pl=plural($diff->y);
                        $age.=$diff->y." year$pl ";
                        break;
                    }
                    if ($diff->m!=0){
                        $pl=plural($diff->m);
                        $age.=$diff->m." month$pl ";
                        break;
                    }
                    if ($diff->d!=0)
                    {
                        $days=$diff->d%7;
                        $weeks=($diff->d-$days)/7;
                        if ($weeks!=0){
                        $pl=plural($weeks);
                        $age.=$weeks." week$pl ";
                        break;
                        }
                        if ($days!=0){
                        $pl=plural($days);
                        $age.=$days." day$pl ";
                        break;
                        }
                    }
                        $age='just now';
                        break;
                    }
                    ?>

                    <li>
                        <section id="post-image" class="col-lg-3">
                        <img src="<?php echo $posts[$current]['image_url']?>" alt="<?php echo $posts[$current]['title']?>">
                        </section>
                        <section id="post-body" class="col-lg-7">
                        <h2><?php echo $posts[$current]['title']?></h2>
                        <p>
                         <?php echo $posts[$current]['body']?>
                        </p>
                        <p>
                         <time datetime="<?php echo $posts[$current]['created_at']?>"> <i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $age ?> ago </time>
                        </p>
                        </section>

                        <section  id="post-author" class="col-lg-2">
                            <div>
                            <img src="<?php echo $authors[$auth_id]['avatar_url']?>" alt="<?php echo $authors[$auth_id]['name']?>">
                            <p id="name">
                                <?php echo $authors[$auth_id]['name']?>
                            </p>
                            <p id="role">
                                <?php echo $authors[$auth_id]['role']?>
                            </p>
                            <p id="place">
                               <i class="fa fa-map-marker" aria-hidden="true"></i>
                               <?php echo $authors[$auth_id]['place']?>
                            </p>
                            </div>
                        </section>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <!-- /.row -->

        <div id="pagination" class="row">
            <div class="col-lg-12 text-center">
                <ul class="pagination">
                <?php
                    if ($page==1){
                        $disabled=' disabled';
                        $link='';
                        echo'<li class="pagnav'.$disabled.'"><a>Previous</a></li>';
                    }
                    else {
                        $index=$page-1;
                        $disabled='';
                        if ($index==1){
                            $link='index.php';
                        }
                        else{
                        $link='page'.$index.'.php';
                        }
                        echo'<li class="pagnav'.$disabled.'"><a href="'.$link.'">Previous</a></li>';
                    }

                for($c=1;sizeof($posts)/PAGINATION+1>$c;$c++){
                    echo '<li';
                    if ($c==$page){
                        echo ' class="active"';
                        $link='';

                    }
                    else {
                        if ($c==1){
                            $link='index.php';
                        }
                        else{
                        $link='page'.$c.'.php';
                        }
                    }
                    echo '><a href="'.$link.'">';
                    echo $c;
                    echo '</a></li>';
                }
                if ($page==ceil(sizeof($posts)/PAGINATION)){
                        $disabled=' disabled';
                        echo '<li class="pagnav'.$disabled.'"><a>Next</a></li>';
                    }
                    else{
                        $index=$page+1;
                        $disabled='';
                        $link='page'.$index.'.php';
                    echo '<li class="pagnav'.$disabled.'"><a href="'.$link.'">Next</a></li>';
                  }
                ?>
                </ul>

            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
