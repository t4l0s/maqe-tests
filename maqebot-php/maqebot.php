<?php

/* problem: maqe-bot
 * source : http://maqe.github.io/maqe-bot.html
 * 
 */

namespace maqe\bot;

/* definitions*/
define('EXT_OK',0);
define('EXT_ERROR',1);
define('DEVMODE','off'); /* switch to on for testing*/
define('INPUT_FILE','input.data');
define('NORTH',1);
define('EAST',2);
define('SOUTH',3);
define('WEST',4);

/* set assert options*/
if (DEVMODE=='on'){
    assert_options(ASSERT_ACTIVE,true);
    assert_options(ASSERT_WARNING,true);
    assert_options(ASSERT_BAIL,true);
}
else
{
    assert_options(ASSERT_ACTIVE,false);
}

class Robot {
    private $posX;
    private $posY;
    private $direction;
    private $dir=Array('North','East','South','West');
    private $commands=array();
    
    function initialize() {
        $this->posX=0;
        $this->posY=0;
        $this->direction=NORTH;
    }

    public function walkTo($instr) {
        /* split the instructions to separate commands*/
        $this->commands=preg_split('/(?<=[0-9])(?=[a-z]+)/i',$instr);
        if (DEVMODE=='on'){
            print_r($this->commands);
        }
        /* loop for each instruction*/
        foreach ($this->commands as $command) {
            /* loop for each command to execute*/
            for($c=0;$c<strlen($command);$c++){ 
                $currentInstr=$command[$c]; 
                switch ($currentInstr){ 
                case 'L':
                    $this->direction--;
                    ($this->direction==0) ? $this->direction=4 : $this->direction;
                    if (DEVMODE=='on'){
                        $this->reportPosition();
                    }
                    break;
                case 'R':
                    $this->direction++;
                    ($this->direction==5) ? $this->direction=1 : $this->direction;
                    if (DEVMODE=='on'){
                        $this->reportPosition();
                    }
                    break;
                case 'W':
                    $steps=intval((substr($command,$c+1)));
                    if ($this->direction>2){ /* bot is facing south or west*/
                        $sign=-1;
                    }
                    else{ /* bot is facing north or east*/
                        $sign=1;
                    }
                    if ($this->direction%2==1){ /* odd number , north or south, change Y only*/
                        $this->posY+=$sign*$steps;
                    }
                    else{ /* even , east or west, change X only*/
                        $this->posX+=$sign*$steps;
                    }           
                    if (DEVMODE=='on'){
                        $this->reportPosition();
                    }
                    /* command ended, terminate command loop*/
                    $c=strlen($command);
                    break;
                    /* switch must never reach this point, unknown command!*/
                default:
                    /* error handling */
                    if ($currentInstr!="\n"){ /* instruction isn't a new line */  
                        /* switch must never reach this point, unknown command!*/
                        $this->unknownCommand();
                    }
                } 
        
            } /* end of commands*/
        } /* end of instructions*/
    } /* end of WalkTo function*/

    
    public function reportPosition() {
        echo 'X: '.$this->posX.' '.'Y: '.$this->posY.' '.'Direction: '.$this->dir[$this->direction-1].PHP_EOL;
    }
    public function unknownCommand() {
        echo 'Unknown command.Stopping...'.PHP_EOL;
    }

    public function checkDirLimits() {
        assert(($this->direction)>0, 'Direction can not be less than 1!');
        assert(($this->direction)<5, 'Direction can not be more than 4!');
    }
}


/* read the data*/
if (!isset($argv[1])){ /* instructions from file */ 
    $inputFile=INPUT_FILE;
    $fh=fopen($inputFile,'r');
    if ($fh==false){
        echo 'Error. Can\'t open file '.$inputFile.PHP_EOL;
        exit(EXT_ERROR);
    }
    $instCount=0;
    while(trim($instructions[$instCount]=fgets($fh))!=''){
        $instCount++;
    }
    fclose($fh);
}
else{ /* instructions from command line */ 
    $instructions[0]=$argv[1];
    $instCount=1;
}

$maqebot= new Robot();
for ($c=0;$c<$instCount;$c++){
    $maqebot->initialize();
    $maqebot->walkTo($instructions[$c]);
    $maqebot->checkDirLimits();
    $maqebot->reportPosition();
}

exit(EXT_OK);
?>