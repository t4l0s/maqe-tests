var http = require('http');
var url = require('url');
var path = require('path');
var bot = require("./bot.js");

var server = http.createServer(function (req, res) {
  var requrl= url.parse(req.url).pathname;

  if (requrl == '/' || path.dirname(requrl)=='/') {
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end("<h1>maqe bot test solved with nodejs</h1><p>Usage: bot.maqetests.tk/maqebot/{instruction string}</p>");
} else if ( path.dirname(requrl)== '/maqebot') {
  var instructions=path.basename(requrl);
  if (instructions==''){
  res.writeHead(200, {"Content-Type": "text/plain"});
  res.end("No instructions provided");
 }
 else {
   res.writeHead(200, {"Content-Type": "text/plain"});
   res.end("got instructions: "+path.basename(requrl)+'\n\n'+bot.solve(instructions));
 }
} else {
  res.writeHead(404, {"Content-Type": "text/html"});
  res.end("<h1>Page does not exist</h1>");
 }
});

server.listen(8888);

console.log("URL: http://127.0.0.1:8888/maqebot/{instruction string}");
