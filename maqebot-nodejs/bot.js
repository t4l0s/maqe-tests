/* solving the instructions problem*/

exports.solve = function(instructions){
var robot= {
    X: 0,
    Y: 0,
  dir: 1,
  err: false,

  dirname: function(dirint){
    switch(dirint){
      case 1:
       return 'North';
      case 2:
       return 'East';
      case 3:
        return 'South';
      case 4:
        return 'West';
    }
  },

  report: function (){
    message='X: '+this.X+' Y: '+this.Y+' Direction: '+this.dirname(this.dir);
  return message;
  },

  abort: function (unknowninstr){
    message='Unknown command '+unknowninstr;
    return message;
  },

  turnLeft: function(){
    this.dir--;
    if (this.dir==0)
      this.dir=4;
  },

  turnRight: function(){
    this.dir++;
    if (this.dir==5)
      this.dir=1;
  },

  walk: function(steps){
    direction=1;
    if (this.dir>2){ /* SOUTH or WEST*/
      direction=-1;
    }
    if (this.dir%2==1){ /* NORTH or SOUTH*/
      this.Y=this.Y+steps*direction;
    }
    else{/* EAST or WEST*/
      this.X=this.X+steps*direction;
    }
  },

  compute: function (instructions){

    for (var c = 0, l = instructions.length, steps=''; c < l; c++) {
      if (instructions[c]=='L'){
        this.turnLeft();
        continue;
      }
      if (instructions[c]=='R'){
        this.turnRight();
        continue;
      }
      if (instructions[c]=='W'){
        c++;
   /* read the number of steps to walk */
 while (instructions[c] >= '0' && instructions[c] <= '9'){
   steps=steps+instructions[c];
   c++;
 }
   c--;
this.walk(steps);
steps='';
continue;
 }
  /* something is wrong, unknown command*/
  this.err=instructions[c];
  return false;
}
  },

 }
if (robot.compute(instructions)==false){
  return robot.abort(robot.err);
}
else{
return robot.report();
 }
}
